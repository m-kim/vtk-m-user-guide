#include <vtkm/filter/ClipWithField.h>
#include <vtkm/filter/ClipWithImplicitFunction.h>
#include <vtkm/filter/MarchingCubes.h>
#include <vtkm/filter/Pathline.h>
#include <vtkm/filter/PointElevation.h>
#include <vtkm/filter/Streamline.h>
#include <vtkm/filter/VertexClustering.h>

#include <vtkm/cont/testing/MakeTestDataSet.h>
#include <vtkm/cont/testing/Testing.h>

namespace
{

////
//// BEGIN-EXAMPLE PointElevation.cxx
////
VTKM_CONT
vtkm::cont::DataSet ComputeAirPressure(vtkm::cont::DataSet dataSet)
{
  vtkm::filter::PointElevation elevationFilter;

  // Use the elevation filter to estimate atmospheric pressure based on the
  // height of the point coordinates. Atmospheric pressure is 101325 Pa at
  // sea level and drops about 12 Pa per meter.
  elevationFilter.SetOutputFieldName("pressure");
  elevationFilter.SetLowPoint(0.0, 0.0, 0.0);
  elevationFilter.SetHighPoint(0.0, 0.0, 2000.0);
  elevationFilter.SetRange(101325.0, 77325.0);

  elevationFilter.SetUseCoordinateSystemAsField(true);

  vtkm::cont::DataSet result = elevationFilter.Execute(dataSet);

  return result;
}
////
//// END-EXAMPLE PointElevation.cxx
////

void DoPointElevation()
{
  std::cout << "** Run elevation filter" << std::endl;

  vtkm::cont::testing::MakeTestDataSet makeData;
  vtkm::cont::DataSet inData = makeData.Make3DRegularDataSet0();

  vtkm::cont::DataSet pressureData = ComputeAirPressure(inData);

  pressureData.GetField("pressure").PrintSummary(std::cout);
  std::cout << std::endl;
}

void DoVertexClustering()
{
  std::cout << "** Run vertex clustering filter" << std::endl;

  vtkm::cont::testing::MakeTestDataSet makeData;
  vtkm::cont::DataSet originalSurface = makeData.Make3DExplicitDataSetCowNose();

  ////
  //// BEGIN-EXAMPLE VertexClustering.cxx
  ////
  vtkm::filter::VertexClustering vertexClustering;

  vertexClustering.SetNumberOfDivisions(vtkm::Id3(128, 128, 128));

  vtkm::cont::DataSet simplifiedSurface = vertexClustering.Execute(originalSurface);
  ////
  //// END-EXAMPLE VertexClustering.cxx
  ////

  simplifiedSurface.PrintSummary(std::cout);
  std::cout << std::endl;
}

void DoClipWithImplicitFunction()
{
  std::cout << "** Run clip with implicit function filter" << std::endl;

  vtkm::cont::testing::MakeTestDataSet makeData;
  vtkm::cont::DataSet inData = makeData.Make3DUniformDataSet0();

  ////
  //// BEGIN-EXAMPLE ClipWithImplicitFunction.cxx
  ////
  // Parameters needed for implicit function
  vtkm::Sphere implicitFunction(vtkm::make_Vec(1, 0, 1), 0.5);

  // Create an instance of a clip filter with this implicit function.
  vtkm::filter::ClipWithImplicitFunction clip;
  clip.SetImplicitFunction(
    vtkm::cont::make_ImplicitFunctionHandle(implicitFunction));

  // By default, ClipWithImplicitFunction will remove everything inside the sphere.
  // Set the invert clip flag to keep the inside of the sphere and remove everything
  // else.
  clip.SetInvertClip(true);

  // Execute the clip filter
  vtkm::cont::DataSet outData = clip.Execute(inData);
  ////
  //// END-EXAMPLE ClipWithImplicitFunction.cxx
  ////

  outData.PrintSummary(std::cout);
  std::cout << std::endl;
}

void DoMarchingCubes()
{
  std::cout << "** Run Marching Cubes filter" << std::endl;

  vtkm::cont::testing::MakeTestDataSet makeData;
  vtkm::cont::DataSet inData = makeData.Make3DRectilinearDataSet0();

  ////
  //// BEGIN-EXAMPLE MarchingCubes.cxx
  ////
  vtkm::filter::MarchingCubes marchingCubes;

  marchingCubes.SetActiveField("pointvar");
  marchingCubes.SetIsoValue(10.0);

  vtkm::cont::DataSet isosurface = marchingCubes.Execute(inData);
  ////
  //// END-EXAMPLE MarchingCubes.cxx
  ////

  isosurface.PrintSummary(std::cout);
  std::cout << std::endl;
}

void DoClipWithField()
{
  std::cout << "** Run clip with field filter" << std::endl;

  vtkm::cont::testing::MakeTestDataSet makeData;
  vtkm::cont::DataSet inData = makeData.Make3DUniformDataSet0();

  ////
  //// BEGIN-EXAMPLE ClipWithField.cxx
  ////
  // Create an instance of a clip filter that discards all regions with scalar
  // value less than 25.
  vtkm::filter::ClipWithField clip;
  clip.SetClipValue(25.0);
  clip.SetActiveField("pointvar");

  // Execute the clip filter
  vtkm::cont::DataSet outData = clip.Execute(inData);
  ////
  //// END-EXAMPLE ClipWithField.cxx
  ////

  outData.PrintSummary(std::cout);
  std::cout << std::endl;
}

void DoStreamlines()
{
  std::cout << "** Run streamlines filter" << std::endl;

  vtkm::cont::DataSetBuilderUniform dataSetBuilder;
  vtkm::cont::DataSetFieldAdd dataSetField;

  vtkm::cont::DataSet inData = dataSetBuilder.Create(vtkm::Id3(5, 5, 5));
  vtkm::Id numPoints = inData.GetCellSet().GetNumberOfPoints();

  vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::FloatDefault, 3>> vectorField;
  vtkm::cont::ArrayCopy(vtkm::cont::make_ArrayHandleConstant(
                          vtkm::Vec<vtkm::FloatDefault, 3>(1, 0, 0), numPoints),
                        vectorField);
  dataSetField.AddPointField(inData, "vectorvar", vectorField);

  ////
  //// BEGIN-EXAMPLE Streamlines.cxx
  ////
  vtkm::filter::Streamline streamlines;

  // Specify the seeds.
  vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::FloatDefault, 3>> seedArray;
  seedArray.Allocate(2);
  seedArray.GetPortalControl().Set(0, vtkm::Vec<vtkm::FloatDefault, 3>(0, 0, 0));
  seedArray.GetPortalControl().Set(1, vtkm::Vec<vtkm::FloatDefault, 3>(1, 1, 1));

  streamlines.SetActiveField("vectorvar");
  streamlines.SetStepSize(0.1f);
  streamlines.SetNumberOfSteps(100);
  streamlines.SetSeeds(seedArray);

  vtkm::cont::DataSet streamlineCurves = streamlines.Execute(inData);
  ////
  //// END-EXAMPLE Streamlines.cxx
  ////

  streamlineCurves.PrintSummary(std::cout);
  std::cout << std::endl;
}

void DoPathlines()
{
  std::cout << "** Run pathlines filter" << std::endl;

  vtkm::cont::DataSetBuilderUniform dataSetBuilder;
  vtkm::cont::DataSetFieldAdd dataSetField;

  vtkm::cont::DataSet inData1 = dataSetBuilder.Create(vtkm::Id3(5, 5, 5));
  vtkm::cont::DataSet inData2 = dataSetBuilder.Create(vtkm::Id3(5, 5, 5));
  vtkm::Id numPoints = inData1.GetCellSet().GetNumberOfPoints();

  vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::FloatDefault, 3>> vectorField1;
  vtkm::cont::ArrayCopy(vtkm::cont::make_ArrayHandleConstant(
                          vtkm::Vec<vtkm::FloatDefault, 3>(1, 0, 0), numPoints),
                        vectorField1);
  dataSetField.AddPointField(inData1, "vectorvar", vectorField1);

  vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::FloatDefault, 3>> vectorField2;
  vtkm::cont::ArrayCopy(vtkm::cont::make_ArrayHandleConstant(
                          vtkm::Vec<vtkm::FloatDefault, 3>(0, 1, 0), numPoints),
                        vectorField2);
  dataSetField.AddPointField(inData2, "vectorvar", vectorField2);

  ////
  //// BEGIN-EXAMPLE Pathlines.cxx
  ////
  vtkm::filter::Pathline pathlines;

  // Specify the seeds.
  vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::FloatDefault, 3>> seedArray;
  seedArray.Allocate(2);
  seedArray.GetPortalControl().Set(0, vtkm::Vec<vtkm::FloatDefault, 3>(0, 0, 0));
  seedArray.GetPortalControl().Set(1, vtkm::Vec<vtkm::FloatDefault, 3>(1, 1, 1));

  pathlines.SetActiveField("vectorvar");
  pathlines.SetStepSize(0.1f);
  pathlines.SetNumberOfSteps(100);
  pathlines.SetSeeds(seedArray);
  pathlines.SetPreviousTime(0.0f);
  pathlines.SetNextTime(1.0f);
  pathlines.SetNextDataSet(inData2);

  vtkm::cont::DataSet pathlineCurves = pathlines.Execute(inData1);
  ////
  //// END-EXAMPLE Pathlines.cxx
  ////

  pathlineCurves.PrintSummary(std::cout);
  std::cout << std::endl;
}

void DoCheckFieldPassing()
{
  std::cout << "** Check field passing" << std::endl;

  vtkm::cont::testing::MakeTestDataSet makeData;
  vtkm::cont::DataSet inData = makeData.Make3DRectilinearDataSet0();

  vtkm::cont::ArrayHandle<vtkm::Float32> scalars;
  vtkm::cont::ArrayCopy(vtkm::cont::ArrayHandleConstant<vtkm::Float32>(
                          1, inData.GetCellSet().GetNumberOfPoints()),
                        scalars);
  vtkm::cont::DataSetFieldAdd::AddPointField(inData, "scalars", scalars);

  vtkm::filter::MarchingCubes filter;

  ////
  //// BEGIN-EXAMPLE SetActiveFieldWithAssociation.cxx
  ////
  filter.SetActiveField("pointvar", vtkm::cont::Field::Association::POINTS);
  ////
  //// END-EXAMPLE SetActiveFieldWithAssociation.cxx
  ////
  filter.SetIsoValue(10.0);

  {
    vtkm::cont::DataSet outData = filter.Execute(inData);
    VTKM_TEST_ASSERT(outData.GetNumberOfFields() == inData.GetNumberOfFields(),
                     "Did not automatically pass all fields.");
  }

  {
    ////
    //// BEGIN-EXAMPLE PassNoFields.cxx
    ////
    filter.SetFieldsToPass(vtkm::filter::FieldSelection::MODE_NONE);
    ////
    //// END-EXAMPLE PassNoFields.cxx
    ////

    vtkm::cont::DataSet outData = filter.Execute(inData);
    VTKM_TEST_ASSERT(outData.GetNumberOfFields() == 0,
                     "Could not turn off passing of fields");
  }

  {
    ////
    //// BEGIN-EXAMPLE PassOneField.cxx
    ////
    filter.SetFieldsToPass("pointvar");
    ////
    //// END-EXAMPLE PassOneField.cxx
    ////

    vtkm::cont::DataSet outData = filter.Execute(inData);
    VTKM_TEST_ASSERT(outData.GetNumberOfFields() == 1,
                     "Could not set field passing correctly.");
    VTKM_TEST_ASSERT(
      outData.HasField("pointvar", vtkm::cont::Field::Association::POINTS));
  }

  {
    ////
    //// BEGIN-EXAMPLE PassListOfFields.cxx
    ////
    filter.SetFieldsToPass({ "pointvar", "cellvar" });
    ////
    //// END-EXAMPLE PassListOfFields.cxx
    ////

    vtkm::cont::DataSet outData = filter.Execute(inData);
    VTKM_TEST_ASSERT(outData.GetNumberOfFields() == 2,
                     "Could not set field passing correctly.");
    VTKM_TEST_ASSERT(
      outData.HasField("pointvar", vtkm::cont::Field::Association::POINTS));
    VTKM_TEST_ASSERT(
      outData.HasField("cellvar", vtkm::cont::Field::Association::CELL_SET));
  }

  {
    ////
    //// BEGIN-EXAMPLE PassExcludeFields.cxx
    ////
    filter.SetFieldsToPass({ "pointvar", "cellvar" },
                           vtkm::filter::FieldSelection::MODE_EXCLUDE);
    ////
    //// END-EXAMPLE PassExcludeFields.cxx
    ////

    vtkm::cont::DataSet outData = filter.Execute(inData);
    VTKM_TEST_ASSERT(outData.GetNumberOfFields() == 1,
                     "Could not set field passing correctly.");
    VTKM_TEST_ASSERT(outData.HasField("scalars"));
  }

  {
    ////
    //// BEGIN-EXAMPLE FieldSelection.cxx
    ////
    vtkm::filter::FieldSelection fieldSelection;
    fieldSelection.AddField("scalars");
    fieldSelection.AddField("cellvar", vtkm::cont::Field::Association::CELL_SET);

    filter.SetFieldsToPass(fieldSelection);
    ////
    //// END-EXAMPLE FieldSelection.cxx
    ////

    vtkm::cont::DataSet outData = filter.Execute(inData);
    VTKM_TEST_ASSERT(outData.GetNumberOfFields() == 2,
                     "Could not set field passing correctly.");
    VTKM_TEST_ASSERT(outData.HasField("scalars"));
    VTKM_TEST_ASSERT(
      outData.HasField("cellvar", vtkm::cont::Field::Association::CELL_SET));
  }

  {
    ////
    //// BEGIN-EXAMPLE PassFieldAndAssociation.cxx
    ////
    filter.SetFieldsToPass("pointvar", vtkm::cont::Field::Association::POINTS);
    ////
    //// END-EXAMPLE PassFieldAndAssociation.cxx
    ////

    vtkm::cont::DataSet outData = filter.Execute(inData);
    VTKM_TEST_ASSERT(outData.GetNumberOfFields() == 1,
                     "Could not set field passing correctly.");
    VTKM_TEST_ASSERT(
      outData.HasField("pointvar", vtkm::cont::Field::Association::POINTS));
  }

  {
    ////
    //// BEGIN-EXAMPLE PassListOfFieldsAndAssociations.cxx
    ////
    filter.SetFieldsToPass(
      { vtkm::make_Pair("pointvar", vtkm::cont::Field::Association::POINTS),
        vtkm::make_Pair("cellvar", vtkm::cont::Field::Association::CELL_SET),
        vtkm::make_Pair("scalars", vtkm::cont::Field::Association::ANY) });
    ////
    //// END-EXAMPLE PassListOfFieldsAndAssociations.cxx
    ////

    vtkm::cont::DataSet outData = filter.Execute(inData);
    VTKM_TEST_ASSERT(outData.GetNumberOfFields() == 3,
                     "Could not set field passing correctly.");
    VTKM_TEST_ASSERT(
      outData.HasField("pointvar", vtkm::cont::Field::Association::POINTS));
    VTKM_TEST_ASSERT(
      outData.HasField("cellvar", vtkm::cont::Field::Association::CELL_SET));
    VTKM_TEST_ASSERT(outData.HasField("scalars"));
  }
}

void Test()
{
  DoPointElevation();
  DoVertexClustering();
  DoClipWithImplicitFunction();
  DoMarchingCubes();
  DoClipWithField();
  DoStreamlines();
  DoPathlines();
  DoCheckFieldPassing();
}

} // anonymous namespace

int ProvidedFilters(int argc, char* argv[])
{
  return vtkm::cont::testing::Testing::Run(Test, argc, argv);
}
