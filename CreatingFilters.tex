% -*- latex -*-

\chapter{Creating Filters}
\label{chap:CreatingFilters}

\index{filter|(}

In Chapter~\ref{chap:Worklets} we discuss how to implement an algorithm in the VTK-m framework by creating a worklet.
Worklets might be straightforward to implement and invoke for those well familiar with the appropriate VTK-m API.
However, novice users have difficulty using worklets directly.
For simplicity, worklet algorithms are generally wrapped in what are called filter objects for general usage.
Chapter~\ref{chap:RunningFilters} introduces the concept of filters and documents those that come with the VTK-m library.
In this chapter we describe how to build new filter objects using the worklet examples introduced in Chapter~\ref{chap:Worklets}.

Unsurprisingly, the base filter objects are contained in the \vtkmfilter{} package.
The basic implementation of a filter involves subclassing one of the base filter objects and implementing the \classmember*{Filter}{DoExecute} method.
The \classmember*{Filter}{DoExecute} method performs the operation of the filter and returns a new data set containing the result.

As with worklets, there are several flavors of filter types to address different operating behaviors although their is not a one-to-one relationship between worklet and filter types.
This chapter is sectioned by the different filter types with an example of implementations for each.

\section{Field Filters}
\label{sec:CreatingFieldFilters}

\index{filter!field|(}
\index{field filter|(}

As described in Section~\ref{sec:ProvidedFieldFilters} (starting on page~\pageref{sec:ProvidedFieldFilters}), field filters are a category of filters that generate a new fields.
These new fields are typically derived from one or more existing fields or point coordinates on the data set.
For example, mass, volume, and density are interrelated, and any one can be derived from the other two.

Field filters are implemented in classes that derive the \vtkmfilter{FilterField} base class.
\textidentifier{FilterField} is a templated class that has a single template argument, which is the type of the concrete subclass.

\begin{didyouknow}
  The convention of having a subclass be templated on the derived class' type is known as the Curiously Recurring Template Pattern (CRTP).
  In the case of \textidentifier{FilterField} and other filter base classes, \VTKm uses this CRTP behavior to allow the general implementation of these algorithms to run \classmember*{Filter}{DoExecute} in the subclass, which as we see in a moment is itself templated.
\end{didyouknow}

All \textidentifier{FilterField} subclasses must implement a \classmember*{Filter}{DoExecute} method.
The \textidentifier{FilterField} base class implements an \classmember*{FilterField}{Execute} method (actually several overloaded versions of \classmember*{FilterField}{Execute}), processes the arguments, and then calls the \classmember*{Filter}{DoExecute} method of its subclass.
The \classmember*{Filter}{DoExecute} method has the following 4 arguments.
\begin{itemize}
\item
  An input data set contained in a \vtkmcont{DataSet} object.
  (See Chapter~\ref{chap:DataSet} for details on \textidentifier{DataSet} objects.)
\item
  The field from the \textidentifier{DataSet} specified in the \classmember*{FilterField}{Execute} method to operate on.
  The field is always passed as an instance of \vtkmcont{ArrayHandle}.
  (See Chapter~\ref{chap:ArrayHandle} for details on \textidentifier{ArrayHandle} objects.)
  The type of the \textidentifier{ArrayHandle} is generally not known until the class is used and requires a template type.
\item
  A \vtkmfilter{FieldMetadata} object that contains the associated metadata of the field not contained in the \textidentifier{ArrayHandle} of the second argument.
  The \textidentifier{FieldMetadata} contains information like the name of the field and what topological element the field is associated with (such as points or cells).
\item
  A policy class.
  \fix{Re-add following line after policies are documented if they are still there.}
  %% (See Chapter~\ref{chap:Policies} for details on policy classes.)
  The type of the policy is generally unknown until the class is used and requires a template type.
\end{itemize}

In this section we provide an example implementation of a field filter that wraps the ``magnitude'' worklet provided in Example~\ref{ex:UseWorkletMapField} (listed on page~\pageref{ex:UseWorkletMapField}).
By convention, filter implementations are split into two files.
The first file is a standard header file with a \textfilename{.h} extension that contains the declaration of the filter class without the implementation.
So we would expect the following code to be in a file named \textfilename{FieldMagnitude.h}.

\vtkmlisting[ex:MagnitudeFilterDeclaration]{Header declaration for a field filter.}{UseFilterField.cxx}

\index{filter!traits|(}
\index{traits!filter|(}

Notice that in addition to declaring the class for \textcode{FieldMagnitude}, Example~\ref{ex:MagnitudeFilterDeclaration} also specializes the \vtkmfilter{FilterTraits} templated class for \textcode{FieldMagnitude}.
The \textidentifier{FilterTraits} class, declared in \vtkmheader{vtkm/filter}{FilterTraits.h}, provides hints used internally in the base filter classes to modify its behavior based on the subclass.
A \textidentifier{FilterTraits} class is expected to define the following types.
\begin{description}
\item[{\classmember*{FilterTraits}{InputFieldTypeList}}]
  A type list containing all the types that are valid for the input field.
  For example, a filter operating on positions in space might limit the types to three dimensional vectors.
  Type lists are discussed in detail in Section~\ref{sec:TypeLists}.
\end{description}

In the particular case of our \textcode{FieldMagnitude} filter, the filter expects to operate on some type of vector field.
Thus, the \classmember{FilterTraits}{InputFieldTypeList} is modified to a list of all standard floating point \textidentifier{Vec}s.

\index{traits!filter|)}
\index{filter!traits|)}

Once the filter class and (optionally) the \textidentifier{FilterTraits} are declared in the \textfilename{.h} file, the implementation filter is by convention given in a separate \textfilename{.hxx} file.
So the continuation of our example that follows would be expected in a file named \textfilename{FieldMagnitude.hxx}.
The \textfilename{.h} file near its bottom needs an include line to the \textfilename{.hxx} file.
This convention is set up because a near future version of \VTKm will allow the building of filter libraries containing default policies that can be used by only including the header declaration.

The implementation of \classmember*{Filter}{DoExecute} is straightforward.
A worklet is invoked to compute a new field array.
\classmember*{Filter}{DoExecute} then returns a newly constructed \vtkmcont{DataSet} object containing the result.
There are numerous ways to create a \textidentifier{DataSet}, but to simplify making filters, \VTKm provides the \vtkmfilterinternal{CreateResult} function to simplify the process.
There are several overloaded versions of \vtkmfilterinternal{CreateResult} (defined in header file \vtkmheader{vtkm/filter/internal}{CreateResult.h}), but the simplest one to use for a filter that adds a field takes the following 5 arguments.
\begin{itemize}
\item
  The input data set.
  This is the same data set passed to the first argument of \classmember*{Filter}{DoExecute}.
\item
  The array containing the data for the new field, which was presumably computed by the filter.
\item
  The name of the new field.
\item
  The topological association (e.g. points or cells) of the new field.
  In the case where the filter is a simple operation on a field array, the association can usually be copied from the \textidentifier{FieldMetadata} passed to \classmember*{Filter}{DoExecute}.
\item
  The name of the element set the new field is associated with.
  This only has meaning if the new field is associated with cells and usually is specified if and only if the new field is associated with cells.
  This name usually can be copied from the \textidentifier{FieldMetadata} passed to \classmember*{Filter}{DoExecute}.
\end{itemize}

Note that all fields need a unique name, which is the reason for the third argument to \textidentifier{CreateResult}.
The \vtkmfilter{FilterField} base class contains a pair of methods named \classmember*{FilterField}{SetOutputFieldName} and \classmember*{FilterField}{GetOutputFieldName} to allow users to specify the name of output fields.
The \classmember*{Filter}{DoExecute} method should respect the given output field name.
However, it is also good practice for the filter to have a default name if none is given.
This might be simply specifying a name in the constructor, but it is worthwhile for many filters to derive a name based on the name of the input field.

\vtkmlisting{Implementation of a field filter.}{FilterFieldImpl.cxx}

\index{field filter|)}
\index{filter!field|)}

\section{Field Filters Using Cell Connectivity}
\label{sec:CreatingFieldFiltersWithCellConnectivity}

\index{filter!field!using cells|(}
\index{field filter using cells|(}

A special subset of field filters are those that take into account the connectivity of a cell set to compute derivative fields.
These types of field filters should be implemented in classes that derive the \vtkmfilter{FilterCell} base class.
\textidentifier{FilterCell} is itself a subclass of \vtkmfilter{FilterField} and behaves essentially the same.
\textidentifier{FilterCell} adds the pair of methods \classmember*{FilterCell}{SetActiveCellSetIndex} and \classmember*{FilterCell}{GetActiveCellSetIndex}.
The \classmember*{FilterCell}{SetActiveCellSetIndex} method allows users to specify which cell set of the given \textidentifier{DataSet} to use.
Likewise, \textidentifier{FilterCell} subclasses should use \classmember*{FilterCell}{GetActiveCellSetIndex} when retrieving a cell set from the given \textidentifier{DataSet}.

Like \textidentifier{FilterField}, \textidentifier{FilterCell} is a templated class that takes as its single template argument the type of the derived class.
Also like \textidentifier{FilterField}, a \textidentifier{FilterCell} subclass must implement a method named \classmember*{Filter}{DoExecute} with 4 arguments: an input \vtkmcont{DataSet} object, a \vtkmcont{ArrayHandle} of the input field, a \vtkmfilter{FieldMetadata} information object, and a policy class.

In this section we provide an example implementation of a field filter on cells that wraps the ``cell center'' worklet provided in Example~\ref{ex:UseWorkletMapPointToCell} (listed on page~\pageref{ex:UseWorkletMapPointToCell}).
By convention, filter implementations are split into two files.
The first file is a standard header file with a \textfilename{.h} extension that contains the declaration of the filter class without the implementation.
So we would expect the following code to be in a file named \textfilename{CellCenter.h}.

\vtkmlisting[ex:CellCenterFilterDeclaration]{Header declaration for a field filter using cell topology.}{UseFilterCell.cxx}

\begin{didyouknow}
  \index{filter!traits} \index{traits!filter} You may have noticed that Example~\ref{ex:MagnitudeFilterDeclaration} provided a specialization for \vtkmfilter{FilterTraits} but Example~\ref{ex:CellCenterFilterDeclaration} provides no such specialization.
  This demonstrates that declaring filter traits is optional.
  If a filter only works on some limited number of types, then it can use \textidentifier{FilterTraits} to specify the specific types it supports.
  But if a filter is generally applicable to many field types, it can simply use the default filter traits.
\end{didyouknow}

Once the filter class and (optionally) the \textidentifier{FilterTraits} are declared in the \textfilename{.h} file, the implementation filter is by convention given in a separate \textfilename{.hxx} file.
So the continuation of our example that follows would be expected in a file named \textfilename{CellCenter.hxx}.
The \textfilename{.h} file near its bottom needs an include line to the \textfilename{.hxx} file.
This convention is set up because a near future version of \VTKm will allow the building of filter libraries containing default policies that can be used by only including the header declaration.

Like with a \textidentifier{FilterField}, a subclass of \textidentifier{FilterCell} implements \classmember*{Filter}{DoExecute} by invoking a worklet to compute a new field array and then return a newly constructed \vtkmcont{DataSet} object.

\vtkmlisting[ex:CellCenterFilterImplementation]{Implementation of a field filter using cell topology.}{FilterCellImpl.cxx}

\begin{commonerrors}
  The policy passed to the \classmember*{Filter}{DoExecute} method contains information on what types of cell sets should be supported by the execution.
  This list of cell set types could be different than the default types specified the \textidentifier{DynamicCellSet} returned from \classmember*{DataSet}{GetCellSet}.
  \index{policy} Thus, it is important to apply the policy to the cell set before passing it to the dispatcher's invoke method.
  The policy is applied by calling the \vtkmfilter{ApplyPolicy} function on the \textidentifier{DynamicCellSet}.
  The use of \textidentifier{ApplyPolicy} is demonstrated in Example~\ref{ex:CellCenterFilterImplementation}.
  \fix{The details of this may change when moving to virtual methods.}
\end{commonerrors}

\index{field filter using cells|)}
\index{filter!field!using cells|)}

\section{Data Set Filters}
\label{sec:CreatingDataSetFilters}

\index{filter!data set|(}
\index{data set filter|(}

As described in Section~\ref{sec:ProvidedDataSetFilters} (starting on page~\pageref{sec:ProvidedDataSetFilters}), data set filters are a category of filters that generate a data set with a new cell set based off the cells of an input data set.
For example, a data set can be significantly altered by adding, removing, or replacing cells.

Data set filters are implemented in classes that derive the \vtkmfilter{FilterDataSet} base class.
\textidentifier{FilterDataSet} is a templated class that has a single template argument, which is the type of the concrete subclass.

All \textidentifier{FilterDataSet} subclasses must implement two methods: \classmember*{FilterDataSet}{DoExecute} and \classmember*{FilterDataSet}{DoMapField}.
The \textidentifier{FilterDataSet} base class implements \classmember*{FilterDataSet}{Execute} and \classmember*{FilterDataSet}{MapFieldOntoOutput} methods that process the arguments and then call the \classmember*{FilterDataSet}{DoExecute} and \classmember*{FilterDataSet}{DoMapField} methods, respectively, of its subclass.

The \classmember*{FilterDataSet}{DoExecute} method has the following 2 arguments.
\begin{itemize}
\item
  An input data set contained in a \vtkmcont{DataSet} object.
  (See Chapter~\ref{chap:DataSet} for details on \textidentifier{DataSet} objects.)
\item
  A policy class.
  \fix{Re-add following line after policies are documented if they are still there.}
  %% (See Chapter~\ref{chap:Policies} for details on policy classes.)
  The type of the policy is generally unknown until the class is used and requires a template type.
\end{itemize}

The \classmember*{FilterDataSet}{DoMapField} method has the following 4 arguments.
\begin{itemize}
\item
  A \vtkmcont{DataSet} object that was returned from the last call to \classmember*{FilterDataSet}{DoExecute}.
  The method both needs information in the \textidentifier{DataSet} object and writes its results back to it, so the parameter should be declared as a non-const reference.
  (See Chapter~\ref{chap:DataSet} for details on \textidentifier{DataSet} objects.)
\item
  The field from the \textidentifier{DataSet} specified in the \classmember*{FilterDataSet}{Execute} method to operate on.
  The field is always passed as an instance of \vtkmcont{ArrayHandle}.
  (See Chapter~\ref{chap:ArrayHandle} for details on \textidentifier{ArrayHandle} objects.)
  The type of the \textidentifier{ArrayHandle} is generally not known until the class is used and requires a template type.
\item
  A \vtkmfilter{FieldMetadata} object that contains the associated metadata of the field not contained in the \textidentifier{ArrayHandle} of the second argument.
  The \textidentifier{FieldMetadata} contains information like the name of the field and what topological element the field is associated with (such as points or cells).
\item
  A policy class.
  \fix{Re-add following line after policies are documented if they are still there.}
  %% (See Chapter~\ref{chap:Policies} for details on policy classes.)
  The type of the policy is generally unknown until the class is used and requires a template type.
\end{itemize}

In this section we provide an example implementation of a data set filter that wraps the functionality of extracting the edges from a data set as line elements.
Many variations of implementing this functionality are given in Chapter~\ref{chap:GeneratingCellSets}.
For the sake of argument, we are assuming that all the worklets required to implement edge extraction are wrapped up in structure named \vtkmworklet{}\textcode{::ExtractEdges}.
Furthermore, we assume that \textcode{ExtractEdges} has a pair of methods, \textcode{Run} and \textcode{ProcessCellField}, that create a cell set of lines from the edges in another cell set and average a cell field from input to output, respectively.
The \textcode{ExtractEdges} may hold state.
All of these assumptions are consistent with the examples in Chapter~\ref{chap:GeneratingCellSets}.

By convention, filter implementations are split into two files.
The first file is a standard header file with a \textfilename{.h} extension that contains the declaration of the filter class without the implementation.
So we would expect the following code to be in a file named \textfilename{ExtractEdges.h}.

\vtkmlisting[ex:ExtractEdgesFilterDoExecute]{Header declaration for a data set filter.}{ExtractEdgesFilterDoExecute.cxx}

Once the filter class and (optionally) the \textidentifier{FilterTraits} are declared in the \textfilename{.h} file, the implementation of the filter is by convention given in a separate \textfilename{.hxx} file.
So the continuation of our example that follows would be expected in a file named \textfilename{ExtractEdges.hxx}.
The \textfilename{.h} file near its bottom needs an include line to the \textfilename{.hxx} file.
This convention is set up because a near future version of \VTKm will allow the building of filter libraries containing default policies that can be used by only including the header declaration.

The implementation of \classmember*{FilterDataSet}{DoExecute} first calls the worklet methods to generate a new \textidentifier{CellSet} class.
It then constructs a \textidentifier{DataSet} containing this \textidentifier{CellSet}.
It also has to pass all the coordinate systems to the new \textidentifier{DataSet}.
Finally, it returns the \textidentifier{DataSet}.

\vtkmlisting[ex:ExtractEdgesFilterDoExecute]{Implementation of the \classmember*{FilterDataSet}{DoExecute} method of a data set filter.}{ExtractEdgesFilterDoExecute.cxx}

The implementation of \classmember*{FilterDataSet}{DoMapField} checks to see what elements the given field is associated with (e.g. points or cells), processes the field data as necessary, and adds the field to the \textidentifier{DataSet}.
The \vtkmfilter{FieldMetadata} passed to \classmember*{FilterDataSet}{DoMapField} provides some features to make this process easier.
\textidentifier{FieldMetadata} contains methods to query what the field is associated with such as \classmember*{FieldMetadata}{IsPointField} and \classmember{FieldMetadata}{IsCellField}.
\textidentifier{FieldMetadata} also has a method named \classmember{FieldMetadata}{AsField} that creates a new \vtkmcont{Field} object with a new data array and metadata that matches the input.

\vtkmlisting[ex:ExtractEdgesFilterDoMapField]{Implementation of the \classmember*{FilterDataSet}{DoMapField} method of a data set filter.}{ExtractEdgesFilterDoMapField.cxx}

\index{data set filter|)}
\index{filter!data set|)}

\section{Data Set with Field Filters}
\label{sec:CreatingDataSetWithFieldFilters}

\index{filter!data set with field|(}
\index{data set with field filter|(}

As described in Section~\ref{sec:ProvidedDataSetWithFieldFilters} (starting on page~\pageref{sec:ProvidedDataSetWithFieldFilters}), data set with field filters are a category of filters that generate a data set with a new cell set based off the cells of an input data set along with the data in at least one field.
For example, a field might determine how each cell is culled, clipped, or sliced.

Data set with field filters are implemented in classes that derive the \vtkmfilter{FilterDataSetWithField} base class.
\textidentifier{FilterDataSetWithField} is a templated class that has a single template argument, which is the type of the concrete subclass.

All \textidentifier{FilterDataSetWithField} subclasses must implement two methods: \classmember*{FilterDataSetWithField}{DoExecute} and \classmember*{FilterDataSetWithField}{DoMapField}.
The \textidentifier{FilterDataSetWithField} base class implements \classmember*{FilterDataSetWithField}{Execute} and \classmember*{FilterDataSetWithField}{MapFieldOntoOutput} methods that process the arguments and then call the \classmember*{FilterDataSetWithField}{DoExecute} and \classmember*{FilterDataSetWithField}{DoMapField} methods, respectively, of its subclass.

The \classmember*{FilterDataSetWithField}{DoExecute} method has the following 4 arguments.
(They are the same arguments for \classmember*{FilterDataSetWithField}{DoExecute} of field filters as described in Section~\ref{sec:CreatingFieldFilters}.)
\begin{itemize}
\item
  An input data set contained in a \vtkmcont{DataSet} object.
  (See Chapter~\ref{chap:DataSet} for details on \textidentifier{DataSet} objects.)
\item
  The field from the \textidentifier{DataSet} specified in the \classmember*{FilterDataSetWithField}{Execute} method to operate on.
  The field is always passed as an instance of \vtkmcont{ArrayHandle}.
  (See Chapter~\ref{chap:ArrayHandle} for details on \textidentifier{ArrayHandle} objects.)
  The type of the \textidentifier{ArrayHandle} is generally not known until the class is used and requires a template type.
\item
  A \vtkmfilter{FieldMetadata} object that contains the associated metadata of the field not contained in the \textidentifier{ArrayHandle} of the second argument.
  The \textidentifier{FieldMetadata} contains information like the name of the field and what topological element the field is associated with (such as points or cells).
\item
  A policy class.
  \fix{Re-add following line after policies are documented if they are still there.}
  %% (See Chapter~\ref{chap:Policies} for details on policy classes.)
  The type of the policy is generally unknown until the class is used and requires a template type.
\end{itemize}

The \classmember*{FilterDataSetWithField}{DoMapField} method has the following 4 arguments.
(They are the same arguments for \classmember*{FilterDataSetWithField}{DoExecute} of field filters as described in Section~\ref{sec:CreatingDataSetFilters}.)
\begin{itemize}
\item
  A \vtkmcont{DataSet} object that was returned from the last call to \classmember*{FilterDataSetWithField}{DoExecute}.
  The method both needs information in the \textidentifier{DataSet} object and writes its results back to it, so the parameter should be declared as a non-const reference.
  (See Chapter~\ref{chap:DataSet} for details on \textidentifier{DataSet} objects.)
\item
  The field from the \textidentifier{DataSet} specified in the \classmember*{FilterDataSetWithField}{Execute} method to operate on.
  The field is always passed as an instance of \vtkmcont{ArrayHandle}.
  (See Chapter~\ref{chap:ArrayHandle} for details on \textidentifier{ArrayHandle} objects.)
  The type of the \textidentifier{ArrayHandle} is generally not known until the class is used and requires a template type.
\item
  A \vtkmfilter{FieldMetadata} object that contains the associated metadata of the field not contained in the \textidentifier{ArrayHandle} of the second argument.
  The \textidentifier{FieldMetadata} contains information like the name of the field and what topological element the field is associated with (such as points or cells).
\item
  A policy class.
  \fix{Re-add following line after policies are documented if they are still there.}
  %% (See Chapter~\ref{chap:Policies} for details on policy classes.)
  The type of the policy is generally unknown until the class is used and requires a template type.
\end{itemize}

In this section we provide an example implementation of a data set with field filter that blanks the cells in a data set based on a field that acts as a mask (or stencil).
Any cell associated with a mask value of zero will be removed.

We leverage the worklets in the \VTKm source code that implement the \textidentifier{Threshold} functionality.
The threshold worklets are templated on a predicate class that, given a field value, returns a flag on whether to pass or cull the given cell.
The \vtkmfilter{Threshold} class uses a predicate that is true for field values within a range.
Our blank cells filter will instead use the predicate class \vtkm{NotZeroInitialized} that will cull all values where the mask is zero.

By convention, filter implementations are split into two files.
The first file is a standard header file with a \textfilename{.h} extension that contains the declaration of the filter class without the implementation.
So we would expect the following code to be in a file named \textfilename{BlankCells.h}.

\vtkmlisting[ex:BlankCellsFilterDeclaration]{Header declaration for a data set with field filter.}{BlankCellsFilterDeclaration.cxx}

Note that the filter class declaration is accompanied by a specialization of \textidentifier{FilterTraits} to specify the field is meant to operate specifically on scalar types.
Once the filter class and (optionally) the \textidentifier{FilterTraits} are declared in the \textfilename{.h} file, the implementation of the filter is by convention given in a separate \textfilename{.hxx} file.
So the continuation of our example that follows would be expected in a file named \textfilename{BlankCells.hxx}.
The \textfilename{.h} file near its bottom needs an include line to the \textfilename{.hxx} file.
This convention is set up because a near future version of \VTKm will allow the building of filter libraries containing default policies that can be used by only including the header declaration.

The implementation of \classmember*{FilterDataSetWithField}{DoExecute} first calls the worklet methods to generate a new \textidentifier{CellSet} class.
It then constructs a \textidentifier{DataSet} containing this \textidentifier{CellSet}.
It also has to pass all the coordinate systems to the new \textidentifier{DataSet}.
Finally, it returns the \textidentifier{DataSet}.

\vtkmlisting[ex:BlankCellsFilterDoExecute]{Implementation of the \classmember*{FilterDataSetWithField}{DoExecute} method of a data set with field filter.}{BlankCellsFilterDoExecute.cxx}

The implementation of \classmember*{FilterDataSetWithField}{DoMapField} checks to see what elements the given field is associated with (e.g. points or cells), processes the field data as necessary, and adds the field to the \textidentifier{DataSet}.
The \vtkmfilter{FieldMetadata} passed to \classmember*{FilterDataSetWithField}{DoMapField} provides some features to make this process easier.
\textidentifier{FieldMetadata} contains methods to query what the field is associated with such as \classmember*{FieldMetaData}{IsPointField} and \classmember*{FieldMetaData}{IsCellField}.
\textidentifier{FieldMetadata} also has a method named \classmember*{FieldMetaData}{AsField} that creates a new \vtkmcont{Field} object with a new data array and metadata that matches the input.

\vtkmlisting[ex:BlankCellsFilterDoMapField]{Implementation of the \classmember*{FilterDataSetWithField}{DoMapField} method of a data set with field filter.}{BlankCellsFilterDoMapField.cxx}

\index{data set with field filter|)}
\index{filter!data set with field|)}

\index{filter|)}
